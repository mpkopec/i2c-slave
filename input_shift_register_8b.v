`timescale 1ns / 1ps

module input_shift_register_8b(
    input clk,
    input rst,
    input serial_in,
    input en,
    output reg [7:0] data
    );

	always @(posedge clk, posedge rst) begin
		if (rst) begin
			data <= 8'h00;
		end
		else begin
			if (en) begin
				data <= {data[6:0], serial_in};
			end
		end
	end

endmodule
