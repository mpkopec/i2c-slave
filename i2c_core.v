`timescale 1ns / 1ps

module i2c_core(
	inout SDA,
	inout SCL,
	input rst,
	input [3:0] dev_addr,
	input [7:0] parallel_in,
	output [7:0] rec_data,
	output [10:0] reg_addr
    );
	 
	wire serial_in, serial_out, r_notWData, r_notWDataMain,
		r_notWDataLine, clk, startAck, i2c_start, i2c_busy, stop_reset;

	sp_detector sp_det (
		.serial_in(serial_in),
		.clk(clk),
		.startAck(startAck),
		.start(i2c_start),
		.busy(i2c_busy),
		.rst(rst)
	);
	
	dataline_split SDA_split (
		.dataline(SDA),
		.r_notW(r_notWData),
		.dataline_out(serial_in),
		.dataline_in(serial_out)
	);
	
	rw_byte rw_line (
		.clk(clk),
		.rst(stop_reset),
		.serial_in(serial_in),
		.serial_outChanged(serial_out),
		.en(en),
		.r_notW(r_notW),
		.ackEn(ackEn),
		.parallel_in(parallel_in),
		.r_notWDataChanged(r_notWDataLine),
		.finished(finished),
		.ackReceived(ackReceived),
		.data(rec_data)
	);
	
	i2c_main_ctrl i2c_ctrl (
		.clk(clk),
		.rst(stop_reset),
		.i2c_start(i2c_start),
		.ackReceived(ackReceived),
		.finished(finished),
		.dev_addr(dev_addr),
		.rec_byte(rec_data),
		.r_notWData(r_notWDataMain),
		.en(en),
		.r_notW(r_notW),
		.ackEn(ackEn),
		.startAck(startAck),
		.reg_addr(reg_addr)
	);
	
	assign stop_reset = rst || ~i2c_busy;
	assign r_notWData = r_notWDataLine | r_notWDataMain;
	assign SCL = 1'bz;
	assign clk = SCL;

endmodule
