`timescale 1ns / 1ps

module output_shift_register_8b(
    input clk,
    input rst,
    input parallel_load,
    input en,
    input [7:0] parallel_in,
	 output serial_out
    );
	 
	reg [7:0] data;

	assign serial_out = data[7];

	always @(negedge clk, posedge rst) begin
		if (rst) begin
			data <= 8'h00;
		end
		else begin
			if (parallel_load) begin
				data <= parallel_in;
			end
			if (en) begin
				data <= data << 1;
			end
		end
	end

endmodule
