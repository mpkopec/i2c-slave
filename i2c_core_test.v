`timescale 20ns / 1ns

module i2c_core_test;

	// Inputs
	reg rst;
	reg [3:0] dev_addr;
	reg [7:0] parallel_in;

	// Outputs
	wire [7:0] rec_data;
	wire [10:0] reg_addr;

	// Bidirs
	wire SDA;
	reg SDAVal;
	wire SCL;
	reg SCLVal;

	// Instantiate the Unit Under Test (UUT)
	i2c_core uut (
		.SDA(SDA), 
		.SCL(SCL), 
		.rst(rst), 
		.dev_addr(dev_addr), 
		.parallel_in(parallel_in), 
		.reg_addr(reg_addr),
		.rec_data(rec_data)
	);
	
	pullup(SDA);
	pullup(SCL);

	initial begin
		// Initialize Inputs
		rst = 1;
		dev_addr = 4'b1111;
		parallel_in = 8'haa;
		SCLVal = 1;
		SDAVal = 1;

		// Wait 100 ns for global reset to finish
		#1 rst = 0;
        
		#200 $finish;
	end
	
	initial begin
		repeat (41) #2 SCLVal = ~SCLVal;
		SCLVal = 1;
		#2 forever #2 SCLVal = ~SCLVal;
	end
	
	initial begin
		#5 SDAVal = 0;
		#2 SDAVal = 1;
		#27 SDAVal = 0;
		#4 SDAVal = 1;
	end
	
	initial begin
		#78 SDAVal = 1;
		#4 SDAVal = 0;
		#4 SDAVal = 1;
	end
	
	initial begin
//		#74 SDAVal = 0;
//		#4 SDAVal = 1;
	end
	
//	initial begin
//		#59 SDAVal = 0;
//		#2 SDAVal = 1;
//	end
	
	assign SDA = (SDAVal) ? 1'bz : 1'b0;
	assign SCL = (SCLVal) ? 1'bz : 1'b0;
      
endmodule

