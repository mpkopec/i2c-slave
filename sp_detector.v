`timescale 1ns / 1ps

module sp_detector(
    input serial_in,
    input clk,
    input rst,
    output reg start,
    output busy,
    input startAck
    );
    
    reg stop;
    
    assign busy = ~stop;
    
    wire start_reset = (rst) || startAck;
    
    always @(posedge start_reset, negedge serial_in) begin
		if (start_reset) begin
			start = 0;
		end
		else begin
			if (clk) 
				start = 1;
			else 
				start = 0;
		end
	end
	
	always @(posedge rst, posedge start, posedge serial_in) begin
		if(rst) begin
			stop = 1;
		end
		else if(start) begin
			stop = 0;
		end
		else begin
			if(clk)       
				stop = 1;
			else
				stop = 0;
		end
	end

endmodule
