`timescale 1ns / 1ps

module rw_byte(
	 input clk,
	 input serial_in,
    input en,
    input r_notW,
    input rst,
	 input ackEn,
    input [7:0] parallel_in,
	 output r_notWDataChanged,
	 output serial_outChanged,
    output finished,
    output ackReceived,
    output [7:0] data
    );
    
    wire serial_outAck, serial_out, r_notWData, data_notACK, finishedFSM;
    
	rw_byte_logic logic(
		.rst(rst),
		.parallel_in(parallel_in),
		.clk(clk),
		.ackEn(ackEn),
		.serial_in(serial_in),
		.r_notW(r_notW),
		.en(en),
		.serial_outAck(serial_outAck),
		.serial_outReg(serial_outReg),
		.data(data),
		.ackReceived(ackReceived),
		.finished(finishedFSM),
		.r_notWData(r_notWData),
		.data_notACK(data_notACK)
	);
	
	multiplexer_2x1 ack_multiplexer (
		.data_notACK(data_notACK),
		.ACK(serial_outAck),
		.data(serial_outReg),
		.SDA_out(serial_out)
	);
	
//	negedge_ff r_notW_changer (
//		.clk(clk),
//		.rst(rst),
//		.D(r_notWData),
//		.out(r_notWDataChanged)
//	);
	
	posedge_ff posedge_finished (
		.D(finishedFSM),
		.clk(clk),
		.rst(rst),
		.out(finished)
	);
	
	assign serial_outChanged = serial_out;
	assign r_notWDataChanged = r_notWData;

endmodule
