`timescale 100ps / 1ps

module sp_detector_test;

	// Inputs
	reg serial_in;
	reg clk;
	reg rst;
	reg startAck;

	// Outputs
	wire start;
	wire busy;

	// Instantiate the Unit Under Test (UUT)
	sp_detector uut (
		.serial_in(serial_in), 
		.clk(clk), 
		.rst(rst), 
		.start(start), 
		.busy(busy), 
		.startAck(startAck)
	);

	initial begin
		// Initialize Inputs
		serial_in = 0;
		clk = 0;
		rst = 1;
		startAck = 0;

		// Wait 100 ns for global reset to finish
		#30 rst = 0;
		#170 $finish;
        
		// Add stimulus here

	end
	
	initial begin
		forever #10 clk = ~clk;
	end
	
	initial begin
		serial_in = 1;
		#35 serial_in = 0;
		#20 startAck = 1;
		#20 startAck = 0;
		#40 serial_in = 1;
	end
      
endmodule

