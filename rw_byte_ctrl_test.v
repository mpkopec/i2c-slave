`timescale 1ns / 1ps

module rw_byte_ctrl_test;

	// Inputs
	reg clk;
	reg rst;
	reg r_notW;
	reg start;
	reg serial_in;

	// Outputs
	wire fwd_notBwd;
	wire finished;
	wire ackReceived;
	wire hold;
	wire r_notWData;
	wire r_notWAck;
	wire parallel_load;
	wire serial_out;

	// Instantiate the Unit Under Test (UUT)
	rw_byte_ctrl uut (
		.clk(clk), 
		.rst(rst), 
		.r_notW(r_notW), 
		.fwd_notBwd(fwd_notBwd), 
		.finished(finished), 
		.ackReceived(ackReceived), 
		.hold(hold), 
		.r_notWData(r_notWData),
		.r_notWAck(r_notWAck), 
		.parallel_load(parallel_load), 
		.start(start), 
		.serial_in(serial_in), 
		.serial_out(serial_out)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 1;
		r_notW = 0;
		start = 0;
		serial_in = 0;
        
		// Add stimulus here
		#3 rst = 0;
	end
	
	initial begin
		forever #1 clk = ~clk;
	end
	
	initial begin
		r_notW = 1;
		start = 1;
		#5 start = 0;
		#30 r_notW = 0;
		#1 start = 1;
		#2 start = 0;
	end
	
	initial begin
		#100 $finish;
	end
      
endmodule

