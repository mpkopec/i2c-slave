`timescale 1ns / 1ps

module posedge_ff(
    input D,
    input clk,
    input rst,
    output reg out
    );

	always @(posedge clk, posedge rst) begin
		if (rst) begin
			out <= 0;
		end
		else begin
			out <= D;
		end
	end

endmodule
