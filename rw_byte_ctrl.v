`timescale 1ns / 1ps
`define WORD_LENGTH 8

module rw_byte_ctrl(
   input clk,
   input rst,
   input r_notW,
   input en,
   input serial_in,
	input ackEn,
   output reg serial_out,
	output reg finished,
	output reg ackReceived,
	output reg in_en,
	output reg out_en,
	output reg data_notACK,
	output reg r_notWData,
	output reg parallel_load
    );

	reg st, nst;
	localparam transmit = 1'd0, ack = 1'd1;
	reg [3:0] ctr;
	reg r_notWSaved;
	
	always @(negedge clk, posedge rst) begin
		if (rst) begin
			st <= transmit;
		end
		else begin
			if (en) begin
				st <= nst;
			end
		end
	end
	
	always @(negedge clk, posedge rst) begin
		if (rst) begin
			r_notWSaved <= 1;
		end
		else begin
			if (en && (ctr == 0 || ctr == 1)) begin
				r_notWSaved <= r_notW;
			end
		end
	end
	
	always @(negedge clk, posedge rst) begin
		if (rst) begin
			ctr <= 4'b0000;
		end
		else begin
			if (en && ctr < `WORD_LENGTH) begin
				ctr <= ctr + 1;
			end
			else if (en && ctr == `WORD_LENGTH) begin
				ctr <= 4'b0000;
			end
		end
	end
	
	always @* begin
		serial_out = 0;
		ackReceived = 0;
		finished = 0;
		parallel_load = 0;
		r_notWData = 1;
		in_en = 0;
		out_en = 0;
		data_notACK = 1;
		nst = transmit;
		
		if (en) begin
			case (st)		
				transmit: begin
					if (r_notWSaved) begin
						r_notWData = 1;
						in_en = 1;
					end
					else begin
						r_notWData = 0;
						out_en = 1;
					end
					
					if (ctr < `WORD_LENGTH) begin
						nst = transmit;
					end
					else begin
						finished = 1;
						parallel_load = 1;
						nst = ack;
					end
				end
				
				ack: begin
					if (r_notWSaved && ackEn) begin
						data_notACK = 0;
						r_notWData = 0;
						serial_out = 0;
					end
					
					if (!r_notWSaved) begin
						r_notWData = 1;
						ackReceived = !serial_in;
					end
					
					parallel_load = 1;
					nst = transmit;
				end
			endcase
		end
	end

endmodule
