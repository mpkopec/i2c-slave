`timescale 1ns / 1ps

module dataline_split(
    input dataline_in,
    input r_notW,
    inout dataline,
    output reg dataline_out
    );

	reg dataline_reg;
	
	always @* begin
		if (r_notW) begin
			dataline_reg = 1'b1;
			assign dataline_out = dataline;
		end
		else begin
			dataline_reg = dataline_in;
		end
	end
	
	assign dataline = (dataline_reg) ? 1'bz : 1'b0;

endmodule
