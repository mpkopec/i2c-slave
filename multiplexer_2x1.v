`timescale 1ns / 1ps

module multiplexer_2x1(
    input data_notACK,
    input ACK,
    input data,
    output SDA_out
    );

	assign SDA_out = (data_notACK) ? data : ACK;

endmodule
