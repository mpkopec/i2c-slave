`timescale 1ns / 1ps

module rw_byte_logic(
    input en,
    input r_notW,
    input serial_in,
    input clk,
    input rst,
	 input ackEn,
    input [7:0] parallel_in,
    output ackReceived,
    output serial_outReg,
    output serial_outAck,
    output data_notACK,
    output finished,
    output r_notWData,
    output [7:0] data
    );
    
    wire parallel_load, out_en, in_en;

	rw_byte_ctrl ctrl (
		.clk(clk),
		.rst(rst),
		.ackEn(ackEn),
		.serial_out(serial_outAck),
		.serial_in(serial_in),
		.en(en),
		.r_notW(r_notW),
		.finished(finished),
		.parallel_load(parallel_load),
		.r_notWData(r_notWData),
		.ackReceived(ackReceived),
		.data_notACK(data_notACK),
		.in_en(in_en),
		.out_en(out_en)
	);
	
	output_shift_register_8b output_register (
		.clk(clk),
		.rst(rst),
		.serial_out(serial_outReg),
		.parallel_in(parallel_in),
		.parallel_load(parallel_load),
		.en(out_en)
	);
	
	input_shift_register_8b input_register (
		.clk(clk),
		.rst(rst),
		.serial_in(serial_in),
		.data(data),
		.en(in_en)
	);

endmodule
