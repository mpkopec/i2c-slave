`timescale 1ns / 1ps

module i2c_main_ctrl(
	input clk,
	input rst,
	input i2c_start,
	input ackReceived,
	input finished,
	input [3:0] dev_addr,
	input [7:0] rec_byte,
	output reg r_notWData,
	output reg en,
	output reg r_notW,
	output reg ackEn,
	output reg startAck,
	output reg [10:0] reg_addr
    );

	reg reg_addrReceived;
	reg ackReceivedSaved;
	reg [3:0] st, nst;
	localparam idle = 4'd0, read_addr = 4'd1, addr_ack = 4'd2, read_reg_addr = 4'd3;
	localparam ack_reg_addr = 4'd4, send_data = 4'd5, read_data_ack = 4'd6;
	localparam wait_for_stop = 4'd7, read_data = 4'd8, send_data_ack = 4'd9;
	
	always @(posedge clk, posedge rst) begin
		if (rst) begin
			st <= idle;
		end
		else begin
			st <= nst;
		end
	end
	
	always @(posedge clk, posedge rst) begin
		if (rst) begin
			reg_addr <= 0;
		end
		else begin
			if (st == read_addr && finished && !rec_byte[0] && rec_byte[7:4] == dev_addr) begin
				reg_addr[10:8] <= rec_byte[3:1];
			end
			else if (st == read_reg_addr && finished) begin
				reg_addr[7:0] <= rec_byte;
			end
		end
	end
	
	always @(posedge clk, posedge rst) begin
		if (rst) begin
			reg_addrReceived <= 0;
		end
		else begin
			if (st == read_reg_addr && finished) begin
				reg_addrReceived <= 1;
			end
		end
	end
	
	always @(posedge clk, posedge rst) begin
		if (rst) begin
			ackReceivedSaved <= 0;
		end
		else begin
			if (nst == read_data_ack && ackReceived) begin
				ackReceivedSaved <= 1;
			end
			else begin
				ackReceivedSaved <= 0;
			end
		end
	end
	
	always @* begin
		r_notWData = 0;
		en = 1;
		r_notW = 1;
		ackEn = 0;
		startAck = 0;
		nst = idle;
		
		case (st)
			idle: begin
				r_notWData = 1;
				
				if (i2c_start) begin
					nst = read_addr;
				end
				else begin
					nst = idle;
				end
			end
			
			read_addr: begin
				startAck = 1;
				
				if (finished) begin
					if (rec_byte[7:4] == dev_addr) begin
						ackEn = 1;
						nst = addr_ack;
					end
					else begin
						nst = idle;
					end
				end
				else begin
					nst = read_addr;
				end
			end
			
			addr_ack: begin
				ackEn = 1;
				if (!rec_byte[0]) begin
					nst = read_reg_addr;
				end
				else begin
					r_notW = 0;
					nst = send_data;
				end
			end
			
			send_data: begin
				r_notW = 0;
				
				if (finished) begin
					nst = read_data_ack;
				end
				else begin
					nst = send_data;
				end
			end
			
			read_data_ack: begin
				r_notW = 0;
				
				if (ackReceivedSaved) begin
					nst = send_data;
				end
				else begin
					nst = wait_for_stop;
				end
			end
			
			read_data: begin
				if (i2c_start) begin
					nst = read_addr;
				end
				else begin
					if (finished) begin
						ackEn = 1;
						nst = send_data_ack;
					end
					else begin
						nst = read_data;
					end
				end
			end
			
			send_data_ack: begin
				ackEn = 1;
				nst = read_data;
			end
			
			wait_for_stop: begin
				nst = wait_for_stop;
			end
			
			read_reg_addr: begin
				if (finished) begin
					ackEn = 1;
					nst = ack_reg_addr;
				end
				else begin
					nst = read_reg_addr;
				end
			end
			
			ack_reg_addr: begin
				ackEn = 1;
				nst = read_data;
			end
			
			default: begin
				nst = idle;
			end
		endcase
	end

endmodule
