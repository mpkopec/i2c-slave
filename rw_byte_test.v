`timescale 1ns / 1ps

module rw_byte_test;

	// Inputs
	reg start;
	reg r_notW;
	reg rst;
	reg ackEn;
	reg [7:0] parallel_in;

	// Outputs
	wire finished;
	wire clk;
	wire ackReceived;
	wire [7:0] data;

	// Bidirs
	wire SCL;
	pullup(SCL);
	reg SCLVal;
	
	wire SDA;
	pullup(SDA);
	reg SDAVal;
	
	// Iteration variables
	integer i;

	// Instantiate the Unit Under Test (UUT)
	rw_byte uut (
		.clk(SCL),
		.serial_in(serial_in),
		.serial_outChanged(serial_outChanged),
		.r_notWDataChanged(r_notWDataChanged),
		.ackEn(ackEn),
		.en(start), 
		.r_notW(r_notW), 
		.rst(rst), 
		.parallel_in(parallel_in), 
		.finished(finished), 
		.ackReceived(ackReceived), 
		.data(data)
	);
	
	dataline_split SDA_splitData (
		.r_notW(r_notWDataChanged),
		.dataline(SDA),
		.dataline_out(serial_in),
		.dataline_in(serial_outChanged)
	);

	initial begin
		// Initialize Inputs
		start = 1;
		r_notW = 1;
		rst = 1;
		ackEn = 0;
		parallel_in = 8'haa;
		SCLVal = 1;
		SDAVal = 1;
		i = 0;

		// Wait 100 ns for global reset to finish
		#3 rst = 0;
		#35 r_notW = 0;
		#35 r_notW = 1;
		#36 r_notW = 0;
        
		// Add stimulus here

	end
	
	initial begin
		#55 SDAVal = 0;
		#2 SDAVal = 1;
	end
	
	initial begin
		#73 SDAVal = 0;
		#2 SDAVal = 1;
	end
	
	initial begin
		#120 start = 0;
	end
	
	initial begin
		#20 forever #1 SCLVal = ~SCLVal;
	end
	
	initial begin
		#150 $finish;
	end
	
	initial begin
//		#4 SDAVal = 1;
//		for (i = 0; i < 7; i = i+1) begin
//			#2 SDAVal = SDAVal;
//		end
//		#2 SDAVal = 1;
//		
//		
//		start = 1;
//		#1 r_notW = 1;
//		#1 SDAVal = 1;
//		#2 start = 0;
//		for (i = 0; i < 6; i = i+1) begin
//			#2 SDAVal = 1;
//		end
//		#2 SDAVal = 0;
//		
//		start = 1;
//		SDAVal = 1;
//		r_notW = 1;
//		#4 start = 0;
//		for (i = 0; i < 6; i = i+1) begin
//			#2 SDAVal = 1;
//		end
//		#2 SDAVal = 1;
//		
//		start = 1;
//		SDAVal = 1;
//		r_notW = 0;
//		#4 start = 0;
//		for (i = 0; i < 6; i = i+1) begin
//			#2 SDAVal = 1;
//		end
//		#2 SDAVal = 1;
//		
//		start = 1;
//		SDAVal = 1;
//		r_notW = 0;
//		#4 start = 0;
//		for (i = 0; i < 6; i = i+1) begin
//			#2 SDAVal = 1;
//		end
//		#2 SDAVal = 1;
//		
//		start = 1;
//		SDAVal = 1;
//		r_notW = 0;
//		#4 start = 0;
//		for (i = 0; i < 6; i = i+1) begin
//			#2 SDAVal = 1;
//		end
//		#2 SDAVal = 1;
	end
	
	always @(posedge finished) begin
//		ackEn = 1;
	end
	
	always @(negedge finished) begin
		if (r_notW) begin
			ackEn = 1;
			#2 ackEn = 0;
		end
	end
	
//	always @(posedge finished) begin
//		#1 start = 1;
//		#2 start = 0;
//	end
	
	assign SCL = (SCLVal) ? 1'bz : 1'b0;
	assign SDA = (SDAVal) ? 1'bz : 1'b0;
	assign clk = SCL;
      
endmodule

