`timescale 1ns / 1ps

module dataline_split_test;

	// Inputs
	reg SDA_in;
	reg r_notW;

	// Outputs
	wire SDA_out;

	// Bidirs
	wire SDA;
	
	reg SDAVal;
	pullup(SDA);
	
	// For iteration purposes	
	integer i;

	// Instantiate the Unit Under Test (UUT)
	dataline_split uut (
		.dataline_out(SDA_out), 
		.dataline_in(SDA_in), 
		.r_notW(r_notW), 
		.dataline(SDA)
	);
	
	initial begin
		#50 $finish;
	end

	initial begin
		SDAVal = 1'b1;
		SDA_in = 0;
		r_notW = 0;

		for (i = 0; i < 10; i = i+1) begin
			#2 SDA_in = ~SDA_in;
		end
		#2 SDA_in = 0;
		r_notW = 1;
		
		for (i = 0; i < 10; i = i+1) begin
			#2 SDAVal = ~SDAVal;
		end
	end
	
	assign SDA = (SDAVal) ? 1'bz : 1'b0;
      
endmodule

